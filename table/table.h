#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

const int N = 8;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    // void another ();

private slots :
    void on_actionRun_triggered();
    void on_actionQuit_triggered();
    // void func ();

    void on_slider_sliderMoved(int position);

private:
    Ui::MainWindow *ui;

private:
    int a [N];
    void display ();
    void place (int k);
};
#endif // MAINWINDOW_H
