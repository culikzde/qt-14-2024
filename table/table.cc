#include "table.h"
#include "ui_table.h"
#include <QApplication>
#include <QTableWidget>

#include <iostream>
using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /*
    QObject::connect (ui->qt4button,
                      SIGNAL (clicked ()),
                      this,
                      SLOT (func ()));

    QObject::connect (ui->qt5button, &QPushButton::clicked,
                      this, &MainWindow::another);

    QObject::connect (ui->qt5button, &QPushButton::clicked,
                      [=] {
                            cout << "LAMBDA" << endl;
                            ui->qt5button->setText ("LAMBDA");
                          }
                      );

    QObject::connect (ui->qt5button, &QPushButton::clicked, &global_func);
    */
}

/*
void MainWindow::another ()
{
    cout << "ANOTHER" << endl;
    this->setWindowTitle ("ANOTHER");
    ui->qt5button->setText ("ANOTHER");
}

void MainWindow::func ()
{
    cout << "FUNC" << endl;
    this->setWindowTitle ("FUNC");
    ui->qt4button->setText ("FUNC");
}

void global_func ()
{
    cout << "GLOBAL" << endl;
}
*/

MainWindow::~MainWindow()
{
    delete ui;
}

QColor columnColor (int k)
{
    return QColor::fromHsl (360*k/N, 220, 120);
}

void MainWindow::display ()
{
    QTableWidget * table = new QTableWidget (ui->tabWidget);

    int cnt = ui->tabWidget->count ();
    ui->tabWidget->addTab (table, QString::number (cnt + 1));

    table->setRowCount (N);
    table->setColumnCount (N);

    int w = table->lineWidth(); // line height
    for (int i = 0; i < N; i++)
        table->setColumnWidth (i, w);

    for (int k = 0; k < N; k++) // k ... cislo sloupce
    {
        int i = a[k]; // i ... cislo radky

        QTableWidgetItem * item = new QTableWidgetItem;

        item->setText (QString::number (k+1));
        item->setBackgroundColor (columnColor (k));

        table->setItem (i, k, item);
    }
}

void MainWindow::place (int k) // umisti damu na k-ty sloupec
{
    for (int i = 0; i < N; i++) // cyklus pro radky
    {
        bool ok = true;
        for (int v = 1; v <= k; v++)
            if (a[k-v] == i || a[k-v] == i-v || a[k-v] == i+v)
                ok = false;
        if (ok)
        {
            a[k] = i; // dama na k-tem sloupci a na i-te radce
            if (k < N-1)
               place (k+1);
            else
               display ();
        }
    }
}

void MainWindow::on_actionRun_triggered()
{
    ui->tabWidget->clear ();
    for (int i = 0; i < N; i++) a[i] = -1;
    place (0);
    int cnt = ui->tabWidget->count ();
    ui->slider->setMaximum (cnt-1);
}

void MainWindow::on_slider_sliderMoved (int pos)
{
   int cnt = ui->tabWidget->count ();
   if (pos >= 0 && pos < cnt)
       ui->tabWidget->setCurrentIndex (pos);

}

void MainWindow::on_actionQuit_triggered()
{
    close ();
}

int main(int argc, char *argv[])
{
    QApplication appl (argc, argv);
    MainWindow w;
    w.show();
    return appl.exec();
}

