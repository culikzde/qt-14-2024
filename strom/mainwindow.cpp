#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>

QString str (qint64 n)
{
    QString s = QString::number (n);
    while (s.length() < 12)
        s = " " + s;
    return s;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QStringList labels { "name", "size", "date" };
    ui->treeWidget->setHeaderLabels (labels);

    QTreeWidgetItem * root = ui->treeWidget->invisibleRootItem();

    QDir dir (".");
    QList <QFileInfo> list = dir.entryInfoList ();
    for (QFileInfo info : list)
    {
        QTreeWidgetItem * branch = new QTreeWidgetItem;
        branch->setText (0, info.fileName());
        branch->setText (1, str (info.size()));
        branch->setText (2, info.lastModified().toString ("dd.MM.yyyy hh:mm:ss"));
        branch->setTextAlignment (1, Qt::AlignRight);
        root->addChild (branch);
    }

    ui->treeWidget->expandAll ();
}

MainWindow::~MainWindow()
{
    delete ui;
}
