#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->treeWidget->header()->hide();

    {
        QTreeWidgetItem * root = new QTreeWidgetItem;
        ui->treeWidget->addTopLevelItem (root);
        root->setText (0, "barvy");
        root->setForeground (0, QColor ("brown"));
        root->setExpanded (true);

        QStringList names = QColor::colorNames();
        for (QString name : names)
        {
            QTreeWidgetItem * branch = new QTreeWidgetItem;
            branch->setText (0, name);
            branch->setForeground (0, QColor (name));
            root->addChild (branch);
        }
    }

    {
        QTreeWidgetItem * root = new QTreeWidgetItem;
        ui->treeWidget->addTopLevelItem (root);
        root->setText (0, "soubory");
        root->setExpanded (true);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
