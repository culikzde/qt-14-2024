﻿#include "inventor.h"

#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoMaterial.h>

#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoShapeHints.h>

// #include <Inventor/manips/SoTrackballManip.h>
#include <Inventor/manips/SoTransformerManip.h>

#include <QWidget>
#include "tree.h"
#include "property.h"

#include <iostream>
#include <vector>
using std::vector;
using std::cout;
using std::endl;

/* ---------------------------------------------------------------------- */

void displaySoNode (QTreeWidgetItem * target, SoNode * node)
{
    TreeItem * branch = new TreeItem;
    branch->so_node = node;

    SoType type = node->getTypeId ();
    branch->setText (0, type.getName().getString());

    if (type.isDerivedFrom (SoGroup::getClassTypeId ()) )
    {
       SoGroup * group = dynamic_cast <SoGroup *> (node);
       int count = group->getNumChildren ();
       for (int i = 0; i < count; i++)
       {
           SoNode * sub_node = group->getChild (i);
           displaySoNode (branch, sub_node);
       }
    }

    target->addChild (branch);
}

/* ---------------------------------------------------------------------- */

void showSoNode (PropertyTable * prop, SoNode * node)
{
   SoFieldContainer * obj = node;

   SoFieldList list;
   obj->getFields (list);
   int len = list.getLength ();

   // display fields
   for (int i = 0; i < len; i++)
   {
       SoField * field = list [i];

       // retrieve field name
       SbName name;
       obj->getFieldName (field, name);

       // retrieve field value
       SbString value;
       field->get (value);

       prop->addText (name.getString (), value.getString ());
   }
}

/* ---------------------------------------------------------------------- */

void simpleScene (SoSeparator * top)
{
    // cone
    SoSeparator * group1 = new SoSeparator;

    SoTransform * shift1 = new SoTransform;
    shift1->translation.setValue (-2.0, 0.0, 0.0);
    group1->addChild (shift1);

    SoMaterial * redMaterial = new SoMaterial;
    redMaterial->diffuseColor.setValue (1.0, 0.0, 0.0);
    group1->addChild (redMaterial);

    SoCone * cone = new SoCone ();
    group1->addChild (cone);

    top->addChild (group1);

    // sphere
    SoSeparator * group2 = new SoSeparator;

    SoTransformerManip * manip = new SoTransformerManip;
    group2->addChild (manip);

    SoMaterial * greenMaterial = new SoMaterial;
    greenMaterial->diffuseColor.setValue (0.0, 1.0, 0.0);
    group2->addChild (greenMaterial);

    SoSphere * sphere = new SoSphere ();
    group2->addChild (sphere);

    top->addChild (group2);

    // cube
    SoSeparator * group3 = new SoSeparator;

    SoTransform * shift3 = new SoTransform ();
    shift3->translation.setValue (1.6, 0.0, 1.6);
    shift3->scaleFactor.setValue (0.7, 0.7, 0.7);
    group3->addChild (shift3);

    SoMaterial * blueMaterial = new SoMaterial;
    blueMaterial->diffuseColor.setValue (0.0, 0.0, 1.0);
    group3->addChild (blueMaterial);

    SoCube * cube = new SoCube ();
    group3->addChild (cube);

    top->addChild (group3);
}

/* ---------------------------------------------------------------------- */

class Color
{
public:
     float r = 1.0, g = 1.0, b = 1.0;

     Color (float r0 = 0, float g0=0, float b0=0) :
         r(r0), g(g0), b(b0)
     { }
};

const Color red    (1.0, 0.0, 0.0);
const Color green  (0.0, 1.0, 0.0);
const Color blue   (0.0, 0.0, 1.0);
const Color yellow (1.0, 0.9, 0.1);

class Point
{
public:
     double x = 0, y = 0, z = 0;
};

typedef vector <Point> PointList;

void displayPoint (SoSeparator * top, Point & p, Color c = yellow)
{
    SoSeparator * group = new SoSeparator;
    top->addChild (group);

    SoTransform * shift = new SoTransform;
    shift->translation.setValue (p.x, p.y, p.z);
    group->addChild (shift);

    SoMaterial * material = new SoMaterial;
    material->diffuseColor.setValue (c.r, c.g, c.b);
    group->addChild (material);

    SoCube * cube = new SoCube ();
    const double mark_size = 0.1;
    cube->width = mark_size;
    cube->height = mark_size;
    cube->depth = mark_size;
    group->addChild (cube);
}

/* ---------------------------------------------------------------------- */

void displayLine (SoSeparator * top, Point a, Point b, Color c = green)
{
    SoSeparator * group = new SoSeparator;
    top->addChild (group);

    SoMaterial * material = new SoMaterial;
    material->diffuseColor.setValue (c.r, c.g, c.b);
    group->addChild (material);

    // http://stackoverflow.com/questions/43034213/how-can-i-draw-a-line-in-open-inventor-3d-graphics-api
    // SoDrawStyle* style = new SoDrawStyle ();
    // style->lineWidth = 3;  // "pixels" but see OpenGL docs
    // group->addChild (style);

    SoVertexProperty * vprop = new SoVertexProperty ();
    vprop->vertex.set1Value (0, a.x, a.y, a.z);  // Set first vertex
    vprop->vertex.set1Value (1, b.x, b.y, b.z);  // Set second vertex

    SoLineSet * line = new SoLineSet();
    line->vertexProperty = vprop;
    group->addChild (line);
}

/* ---------------------------------------------------------------------- */

void displayPolygon (SoSeparator * top, PointList & points, Color c = blue)
{
    SoSeparator * group = new SoSeparator;
    top->addChild (group);

    SoShapeHints * hints = new SoShapeHints;
    hints->shapeType.setValue(SoShapeHints::UNKNOWN_SHAPE_TYPE);
    // NO hints->shapeType.setValue(SoShapeHints::SOLID);
    hints->faceType.setValue(SoShapeHints::CONVEX);
    // NO hints->vertexOrdering.setValue(SoShapeHints::CLOCKWISE);
    hints->vertexOrdering.setValue(SoShapeHints::COUNTERCLOCKWISE);
    group->addChild (hints);

    int cnt = points.size ();

    float vertices [cnt][3];
    for (int i = 0 ; i < cnt; i++)
    {
        Point & p = points [i];
        float * t = vertices [i];
        t[0] = p.x;
        t[1] = p.y;
        t[2] = p.z;
    }

    int32_t numvertices [1] = {cnt};

    SoVertexProperty *myVertexProperty = new SoVertexProperty;

   // Define the normals used:
   // myVertexProperty->normal.setValues(0, 8, norms);
   // myVertexProperty->normalBinding = SoNormalBinding::PER_FACE;

   // Define material
   myVertexProperty->orderedRGBA.setValue (SbColor(c.r, c.g, c.b).getPackedValue());

   // Define coordinates for vertices
   myVertexProperty->vertex.setValues(0, cnt, vertices);

   // Define the FaceSet
   SoFaceSet *myFaceSet = new SoFaceSet;
   myFaceSet->numVertices.setValues(0, 1, numvertices);

   myFaceSet->vertexProperty.setValue(myVertexProperty);
   group->addChild(myFaceSet);
}

/* ---------------------------------------------------------------------- */

void displayBoard (SoSeparator * top)
{
    const int N = 8;
    Point p [N+1] [N+1];
    for (int i = 0; i <= N  ; i++)
        for (int k = 0; k <= N; k++)
        {
            p[i][k].x = i+2;
            p[i][k].y = k+2;
        }

    for (int i = 0; i <= N  ; i++)
        for (int k = 0; k <= N; k++)
            displayPoint (top, p[i][k]);

    for (int i = 0; i <= N  ; i++)
        for (int k = 0; k <= N; k++)
        {
            if (i < N) displayLine (top, p[i][k], p[i+1][k]);
            if (k < N) displayLine (top, p[i][k], p[i][k+1]);

        }

    for (int i = 0; i < N ; i++)
        for (int k = 0; k < N; k++)
        {
            PointList list { p[i][k], p[i+1][k], p[i+1][k+1], p[i][k+1] };
            displayPolygon (top, list);
        }
}

/* ---------------------------------------------------------------------- */

void add_inv (SoSeparator * top)
{
    /*
    SoTransform * shift = new SoTransform;
    shift->translation.setValue (0.0, 19.0, 0.0);
    top->addChild (shift);

    SoTransform * scale = new SoTransform;
    scale->scaleFactor.setValue (1.0, -1.0, 1.0);
    top->addChild (scale);
    */
}

void displayCylinder (SoSeparator * top, Point & p, float height, Color c, bool inv)
{
    SoSeparator * group = new SoSeparator;
    top->addChild (group);

    SoTransform * shift = new SoTransform;
    shift->translation.setValue (p.x+0.5, p.y+height/2, p.z+0.5);
    group->addChild (shift);

    // cout << "cylinder " << p.z << " " << p.y << " " << p.x << " " << int (inv) << endl;
    if (inv) add_inv (top);

    SoMaterial * material = new SoMaterial;
    material->diffuseColor.setValue (c.r, c.g, c.b);
    group->addChild (material);

    SoCylinder * cylinder = new SoCylinder ();
    group->addChild (cylinder);
    cylinder->height = height;
    cylinder->radius = 0.4;
}

void displayBox (SoSeparator * top, Point & p, float height, Color c, bool inv)
{
    SoSeparator * group = new SoSeparator;
    top->addChild (group);

    SoTransform * shift = new SoTransform;
    shift->translation.setValue (p.x+0.5, p.y+height/2, p.z+0.5);
    group->addChild (shift);

    add_inv (top);

    SoMaterial * material = new SoMaterial;
    material->diffuseColor.setValue (c.r, c.g, c.b);
    group->addChild (material);

    SoCube * cube = new SoCube ();
    group->addChild (cube);
    cube->height = height;
    cube->depth = 0.4;
    cube->width = 0.4;
}

void displayCone (SoSeparator * top, Point & p, float height, Color c, bool inv)
{
    SoSeparator * group = new SoSeparator;
    top->addChild (group);

    SoTransform * shift = new SoTransform;
    shift->translation.setValue (p.x+0.5, p.y+height/2, p.z+0.5);
    group->addChild (shift);


    if (inv) add_inv (top);

    SoMaterial * material = new SoMaterial;
    material->diffuseColor.setValue (c.r, c.g, c.b);
    group->addChild (material);

    SoCone * cone = new SoCone ();
    group->addChild (cone);
    cone->height = height;
    cone->bottomRadius = 0.25;
}

void displaySphere (SoSeparator * top, Point & p, float height, Color c, bool inv)
{
    SoSeparator * group = new SoSeparator;
    top->addChild (group);

    SoTransform * shift = new SoTransform;
    shift->translation.setValue (p.x+0.5, p.y+3*height/4, p.z+0.5);
    group->addChild (shift);

    if (inv) add_inv (top);

    SoTransform * scale = new SoTransform;
    scale->scaleFactor.setValue (0.5, 2*height, 0.5);
    group->addChild (scale);

    SoMaterial * material = new SoMaterial;
    material->diffuseColor.setValue (c.r, c.g, c.b);
    group->addChild (material);

    SoSphere * sphere = new SoSphere ();
    group->addChild (sphere);
    sphere->radius = 0.4;
}

/* ---------------------------------------------------------------------- */

const int N = 8;
Point p [N+1] [N+1] [N+1];

void displaySet (SoSeparator * top, int t, int i1, int i2, Color c, bool inv)
{
    for (int k = 0; k < N ; k++)
    {
        // cout << t << " " << i1 << " " << k << " " << inv << endl;
        displayCylinder (top, p[t][i1][k], 0.2, c, inv);
    }

    displayBox    (top, p[t][i2][0], 0.5, c, inv);
    displaySphere (top, p[t][i2][1], 0.5, c, inv);
    displayCone   (top, p[t][i2][2], 0.5, c, inv);
    displayCone   (top, p[t][i2][3], 1.0, c, inv);
    displayBox    (top, p[t][i2][4], 1.0, c, inv);
    displayCone   (top, p[t][i2][5], 0.5, c, inv);
    displaySphere (top, p[t][i2][6], 0.5, c, inv);
    displayBox    (top, p[t][i2][7], 0.5, c, inv);
}

void displayBoard3 (SoSeparator * top)
{
    for (int t = 0; t <= N  ; t++)
        for (int i = 0; i <= N  ; i++)
             for (int k = 0; k <= N; k++)
             {
                p[t][i][k].x = i+2;
                p[t][i][k].y = t+2;
                p[t][i][k].z = k+2;
             }

    for (int t = 0; t <= N  ; t++)
        for (int i = 0; i <= N  ; i++)
            for (int k = 0; k <= N; k++)
                displayPoint (top, p[t][i][k]);

    for (int t = 0; t <= N ; t++)
        for (int i = 0; i <= N; i++)
            for (int k = 0; k <= N; k++)
            {
                if (t < N) displayLine (top, p[t][i][k], p[t+1][i][k]);
                if (i < N) displayLine (top, p[t][i][k], p[t][i+1][k]);
                if (k < N) displayLine (top, p[t][i][k], p[t][i][k+1]);
            }

    for (int i = 0; i < N ; i++)
        for (int k = 0; k < N; k++)
        {
            PointList list { p[0][i][k], p[0][i+1][k], p[0][i+1][k+1], p[0][i][k+1] };
            displayPolygon (top, list, (i+k) % 2 == 0 ? blue : red);
        }

    displaySet (top, 0, 1, 0, blue, false);
    displaySet (top, 0, N-2, N-1, red, false);

    displaySet (top, N-1, 1, 0, yellow, true);
    displaySet (top, N-1, N-2, N-1, green, true);
}

/* ---------------------------------------------------------------------- */

void inventorScene (QTreeWidget * tree, QTabWidget *tabs, PropertyTable * prop)
{
    QWidget * widget = new QWidget (tabs);
    tabs->addTab (widget, "Inventor");

    SoQtExaminerViewer * examiner = new SoQtExaminerViewer (widget);

    SoSeparator * root = new SoSeparator;

    simpleScene (root);
    displayBoard (root);
    displayBoard3 (root);

    examiner->setSceneGraph(root);

    displaySoNode (tree->invisibleRootItem (), root);
}

/* ---------------------------------------------------------------------- */

#if 0
void MainWindow::on_tree_itemActivated (QTreeWidgetItem * param, int column)
{
   TreeItem * item = dynamic_cast <TreeItem *> (param);
   if (item != nullptr)
      displayNode (item->node);
}
#endif

#if 0
int main (int argc, char ** argv)
{
  QApplication app (argc, argv);

  SoQt::init ((QWidget*)NULL);

  MainWindow * win = new MainWindow();
  win->show();

  QObject::connect (&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));

  SoQt::mainLoop();

  return 0;
}
#endif
