
from __future__ import print_function

try :
   from SocketServer import TCPServer
   from SimpleHTTPServer import SimpleHTTPRequestHandler
except :
   from socketserver import TCPServer
   from http.server import SimpleHTTPRequestHandler

import sys, socket, json

PORT = 1234
# https://www.jeasyui.com/tutorial/datagrid/datagrid20.php

tree = [
              {	 "id":1,
                 "text":"block",
                 "children" : [ { "id":2, "text":"left" }, { "id":3, "text":"right" } ]
              },
              {	"id":4,	"text":"line" }
       ]

table = {
            "total" : 3,
            "rows" :
            [
                { "name": "block", "type": "rectangle",
                  "x": 97, "y": 27, "width": 200, "height": 160,
                  "pen": "orange", "brush": "yellow", },

                { "name": "left", "type": "ellipse",
                  "x": 40, "y": 40, "width": 40, "height": 40,
                  "pen": "cornflowerblue", "brush": "blue" },

                { "name": "right", "type": "ellipse",
                  "x": 120, "y": 40, "width": 40, "height": 40,
                  "pen": "cornflowerblue", "brush": "blue" },
            ]
        }

properties = [
               { "name": "Name",  "value" : "Abc", "group": "first" },
               { "name": "Value", "value" : 7, "group": "first", "editor":"numberbox"},
               { "name": "Checked", "value" : True, "group": "second" },
             ]


def str_to_bytes (s) :
    if sys.version_info >= (3,) :
       return bytes (s, "ascii")
    else :
       return bytes (s)


class MyHandler (SimpleHTTPRequestHandler):

      def do_GET(self):
          if self.path=="/tree.json":
             self.send_response (200)
             self.send_header ("Content-type", "application/json")
             self.end_headers ()
             self.wfile.write (str_to_bytes(json.dumps(tree)))
             return
          if self.path=="/table.json":
             self.send_response (200)
             self.send_header ("Content-type", "application/json")
             self.end_headers ()
             self.wfile.write (str_to_bytes(json.dumps(table)))
             return
          if self.path=="/properties.json":
             self.send_response (200)
             self.send_header ("Content-type", "application/json")
             self.end_headers ()
             self.wfile.write (str_to_bytes(json.dumps(properties)))
             return
          SimpleHTTPRequestHandler.do_GET (self)


server = TCPServer (("", PORT), MyHandler)
server.socket.setsockopt (socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

try :
   print ("serving at port", PORT)
   server.serve_forever ()
except KeyboardInterrupt :
   print ("Control C received, shutting down the web server")
finally :
   server.socket.close ()

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

