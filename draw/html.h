
/* html.h */

#ifndef HTML_H
#define HTML_H

// #ifdef QT_WEBKITWIDGETS_LIB
//    #define USE_WEB_KIT
// #endif

#ifdef USE_WEB_KIT
   #include <QWebView>
#else
   #include <QWebEngineView>
#endif

#include <QVBoxLayout>
#include <QToolBar>
#include <QStatusBar>
#include <QLineEdit>

class Win;

class HtmlView : public QWidget
{
   Q_OBJECT
   public:
      HtmlView (Win * win_param);
      void load(QUrl url);

    private slots:
       void adjustLocation ();
       void changeLocation ();
       void setProgress (int p);

    private:
       #ifdef USE_WEB_KIT
          QWebView * view;
       #else
          QWebEngineView * view;
       #endif

       QVBoxLayout * layout;
       QToolBar * toolbar;
       QLineEdit * locationEdit;

       Win * win;
       QStatusBar * statusbar;
};

#endif // HTML_H

