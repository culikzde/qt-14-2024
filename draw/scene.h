#ifndef SCENE_H
#define SCENE_H

#include "precompiled.h"

class MainWindow;
class Source;
class Target;

class Scene : public QGraphicsScene
{
public:
    Scene (MainWindow * window);

protected:
    void dragEnterEvent (QGraphicsSceneDragDropEvent *event) override;
    void dragMoveEvent(QGraphicsSceneDragDropEvent *event) override;
    void dropEvent (QGraphicsSceneDragDropEvent *event) override;

    void sceneBackground ();

    // bool event (QEvent * e) override;
    void onSelectionChanged ();

private:
    MainWindow * win;
    void createSourceAndTarget (Source * & source, Target * & target);
    void place (QGraphicsItem * tem, QGraphicsItem * location, QPointF point);
    void placeAtPoint (QGraphicsItem * item, QPointF point);

    void moveUp (QGraphicsItem * item);
    void moveDown (QGraphicsItem * item);
    void linkItem (QGraphicsItem * item, QPointF point);

    QPointF startPos;

protected:

    void contextMenuEvent (QGraphicsSceneContextMenuEvent * event) override;

    void mousePressEvent (QGraphicsSceneMouseEvent * event) override;
    void mouseReleaseEvent (QGraphicsSceneMouseEvent * event) override;

    void mouseDoubleClickEvent (QGraphicsSceneMouseEvent  *event) override;

private :
    int relCnt = 0;

    // only for input/output
    QMap < int, int > renameMap;
    QMap < int, Source * > sourceMap;
    QMap < int, Target * > targetMap;
    QList < Source * > sourceList;
    QList < Target * > targetList;

public:
    void initInput ();
    int  renameRel (int orig_rel);
    void storeSource (Source * source);
    void storeTarget (Target * target);
    void completeInput ();
};

QGraphicsItem * itemFromTool (QString tool);

void drawBoard (QGraphicsScene *);

#endif // SCENE_H
