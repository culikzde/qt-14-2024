#ifndef QT_VIEW_H
#define QT_VIEW_H

#include <QWidget>
#include <QTreeWidget>

/* ---------------------------------------------------------------------- */

void display_qt_classes (QTreeWidget * tree);

/* ---------------------------------------------------------------------- */

#endif // QT_VIEW_H
