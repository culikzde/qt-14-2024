#include "scene.h"
#include "block.h"
#include "circle.h"
#include "port.h"
#include "toolbutton.h"
#include "draw.h"
#include "io.h"

#ifdef USE_PANEL
   #include "panel.h"
#endif

Scene::Scene (MainWindow * window) :
    win (window)
{
    connect (this, &QGraphicsScene::selectionChanged, this, &Scene::onSelectionChanged);
    sceneBackground ();
}

void Scene::sceneBackground ()
{
    const int n = 16;
    QBitmap texture (n, n);
    texture.clear ();

    QPainter painter (&texture);
    painter.drawLine (0, 0, n-1, 0);
    painter.drawLine (0, 0, 0, n-1);
    // painter.drawLine (0, 0, n-1, n-1);
    // painter.drawLine (0, n-1, n-1, 0);
    painter.end ();

    QBrush brush ("cornflowerblue");
    brush.setTexture (texture);
    setBackgroundBrush (brush);
}

void Scene::onSelectionChanged ()
{
    // win->showInfo ("SELECTION");
    QList<QGraphicsItem *> list = selectedItems ();
    if (list.count () == 1)
        win->showProperties (list [0]);
}

/*
bool Scene::event (QEvent * e)
{
    if (e->type () == QEvent::GraphicsSceneMove)
        win->showInfo ("MOVE");

    return QGraphicsScene::event (e);
}
*/

void Scene::dragEnterEvent (QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * data = event->mimeData();
    event->setAccepted (data->hasColor() || data->hasFormat (toolFormat));
}

void Scene::dragMoveEvent (QGraphicsSceneDragDropEvent * event)
{
    // important
}

void Scene::dropEvent (QGraphicsSceneDragDropEvent *event)
{
    const QMimeData * data = event->mimeData();
    if (data->hasColor())
    {
        QColor color = data->colorData().value <QColor> ();
        QPointF pos = event->scenePos ();
        QGraphicsItem * item = itemAt (pos, QTransform ());
        if (item == nullptr)
        {
            QBrush brush = backgroundBrush();
            brush.setColor (color);
            setBackgroundBrush (brush);
        }
        else if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
        {
            if (event->proposedAction() == Qt::CopyAction) // Ctrl Mouse
                shape->setPen (color);
            else
                shape->setBrush (color);
        }
        else if (auto line = dynamic_cast < QGraphicsLineItem * > (item))
        {
            QPen pen = line->pen ();
            pen.setColor (color);
            line->setPen (pen);
        }
    }
    else if (data->hasFormat (toolFormat))
    {
        QString tool = data->data (toolFormat);
        QGraphicsItem * item = itemFromTool (tool);

        QPointF p = event->scenePos ();
        QGraphicsItem * target = itemAt (p, QTransform ());

        if (target == nullptr)
        {
           addItem (item);
           item->setPos (p);
        }
        else
        {
           item->setParentItem (target);
           p = target->mapFromScene (p);
           item->setPos (p);
        }

        win->refreshTree ();
    }
}

/* ---------------------------------------------------------------------- */

QGraphicsItem * itemFromTool (QString tool)
{
    QGraphicsItem * result = nullptr;

    if (tool == "rectangle")
    {
        result = new Block;
    }
    else if (tool == "ellipse")
    {
        result = new Circle;
    }
    else if (tool == "line")
    {
        result = createConnection ();
    }

    #ifdef USE_PANEL
    else
    {
       result = createComponent (tool);
    }
    #endif

    /*
    {
        TypeDesc * desc = findDescription (tool);
        if (desc != nullptr)
           result = desc->createItem ();
    }
    */

    /*
    if (result != nullptr)
    {
       result->setFlags (QGraphicsItem::ItemIsMovable  | QGraphicsEllipseItem::ItemIsSelectable);
    }
    */

    return result;
}

void Scene::createSourceAndTarget (Source * & source, Target * & target)
{
    source = new Source ();
    target = new Target ();

    ConnectionLine * line = new ConnectionLine;
    line->setPen (QColor ("red"));
    line->setParentItem (source);

    source->line = line;
    source->target = target;
    target->source = source;

    relCnt ++;
    source->rel = relCnt;
    target->rel = relCnt;

    source->setToolTip ("source");
    target->setToolTip ("target");
    line->setToolTip ("line");
}

void Scene::place (QGraphicsItem * item, QGraphicsItem * location, QPointF point)
{
    if (location != nullptr)
    {
        point = location->mapFromScene (point);
        item->setPos (point);
        item->setParentItem (location);
    }
    else
    {
        item->setPos (point);
        this->addItem (item);
    }
}

void Scene::placeAtPoint (QGraphicsItem * item, QPointF point)
{
    QGraphicsItem * location = itemAt (point, QTransform ());
    place (item, location, point);
}

/* ---------------------------------------------------------------------- */

void Scene::contextMenuEvent (QGraphicsSceneContextMenuEvent * event)
{
    QPointF point = event->scenePos ();
    QGraphicsItem * location = itemAt (point, QTransform ());
    if (location != nullptr)
    {
        QMenu * menu = new QMenu ();
        menu->addAction ("move up", this, [=] { moveUp (location); });
        menu->addAction ("move down", this, [=] { moveDown (location); });
        menu->addAction ("link", this, [=] { linkItem (location, point); });
        #if USE_PANEL
        if (Panel * panel = dynamic_cast < Panel * > (location))
        {
           panel->contextMenu (menu);
        }
        #endif
        menu->exec (QCursor::pos());
    }
}

void Scene::moveUp (QGraphicsItem * item)
{
    item->setZValue (item->zValue() + 1);
}

void Scene::moveDown (QGraphicsItem * item)
{
    item->setZValue (item->zValue() - 1);
}

void Scene::linkItem (QGraphicsItem * item, QPointF point)
{
    QGraphicsItem * result = nullptr;
    for (QGraphicsItem * t : items (point))
        if (t != item && result == nullptr)
            result = t;
    if (result != nullptr)
    {
        QPointF pos = QPointF (0, 0);
        pos = result->mapFromItem (item, pos);
        item->setParentItem (result);
        item->setPos (pos);
    }
}

/* ---------------------------------------------------------------------- */

void Scene::mousePressEvent (QGraphicsSceneMouseEvent * event)
{
    Qt::MouseButton button = event->button () ;
    Qt::KeyboardModifiers modif = event->modifiers ();
    if (button == Qt::MiddleButton || button == Qt::LeftButton && modif == Qt::SHIFT)
    {
       startPos = event->scenePos();
    }
    else
    {
       QGraphicsScene::mousePressEvent (event);
    }
}

void Scene::mouseReleaseEvent (QGraphicsSceneMouseEvent * event)
{
    Qt::MouseButton button = event->button () ;
    Qt::KeyboardModifiers modif = event->modifiers ();
    if (button == Qt::MiddleButton || button == Qt::LeftButton && modif == Qt::SHIFT)
    {
        QPointF stopPos = event->scenePos();

        Source * source;
        Target * target;
        createSourceAndTarget (source, target);
        placeAtPoint (source, startPos);
        placeAtPoint (target, stopPos);
        target->updateLine ();

        win->refreshTree ();
    }
    else
    {
        QGraphicsScene::mouseReleaseEvent (event);
    }
}

void Scene::mouseDoubleClickEvent (QGraphicsSceneMouseEvent * event)
{
    /*
    QPointF point = event->scenePos ();
    QGraphicsItem * location = itemAt (point, QTransform ());

    for (QGraphicsItem * item : selectedItems ())
        item->setSelected (false);

    if (location != nullptr)
        location->setSelected (true);
    */
}

/* ---------------------------------------------------------------------- */

void Scene::initInput ()
{
   renameMap.clear ();
   sourceMap.clear ();
   targetMap.clear ();
   sourceList.clear ();
   targetList.clear ();
}

int Scene::renameRel (int orig_rel)
{
    int rel = 0;
    if (orig_rel != 0)
    {
        if (renameMap.contains(orig_rel))
        {
            rel = renameMap [orig_rel];
        }
        else
        {
            relCnt ++;
            rel = relCnt;
            renameMap [orig_rel] = rel;
        }
    }
    return rel;
}

void Scene::storeSource (Source * source)
{
    source->rel = renameRel (source->rel);
    if (source->rel != 0)
       sourceMap [source->rel] = source;
    sourceList.append (source);
}

void Scene::storeTarget (Target * target)
{
    target->rel = renameRel (target->rel);
    if (target->rel != 0)
       targetMap [target->rel] = target;
    targetList.append (target);
}

void Scene::completeInput ()
{
    for (Source * s : sourceList)
        if (targetMap.contains(s->rel))
            s->target = targetMap [s->rel];

    for (Target * t : targetList)
        if (sourceMap.contains(t->rel))
            t->source = sourceMap [t->rel];

    for (Source * s : sourceList)
        for (QGraphicsItem * item : s->childItems ())
            if (ConnectionLine * line = dynamic_cast <ConnectionLine *> (item))
            {
                s->line = line;
                if (s->target != nullptr)
                   s->target->updateLine ();
            }

    renameMap.clear ();
    sourceMap.clear ();
    targetMap.clear ();
    sourceList.clear ();
    targetList.clear ();
}

/* ---------------------------------------------------------------------- */

class GameBoard
{
public:
    static const int N = 4;
    void draw (QGraphicsScene * p_scene);
private:
    QGraphicsPolygonItem * board [3][N][2*N];
    QGraphicsScene * scene = nullptr;

    void line (QPointF A, QPointF B, QPointF AB[N+1]);
    void addPawn (int z, int x, int y, QColor c);
    void addPoint (QPointF p);
    void drawSection (QPointF AB[N], QPointF CD[N]);
};

/* ---------------------------------------------------------------------- */

const double sqrt3_2 = std::sqrt (3) / 2;

const double a = 80;

double sin30 (int n)
{
    switch (n % 12)
    {
        case 0: return 0; break;
        case 1: return 0.5; break;
        case 2: return sqrt3_2; break;
        case 3: return 1; break;
        case 4: return sqrt3_2; break;
        case 5: return 0.5; break;
        case 6: return 0; break;
        case 7: return - 0.5; break;
        case 8: return - sqrt3_2; break;
        case 9: return - 1; break;
        case 10: return - sqrt3_2; break;
        case 11: return - 0.5; break;
    }
    return 0;
}

inline double cos30 (int n)
{
    return sin30 (n+3);
}

QPointF rot30 (int x, int y, int n) /* n=1 ... 30 stupnu */
{
    return QPointF (cos30(n)*x + sin30(n)*y, -sin30(n)*x + cos30(n)*y);
}

QPointF rot30 (QPointF p, int n) /* n=1 ... 30 stupnu */
{
    return rot30 (p.x(), p.y(), n);
}

/* ---------------------------------------------------------------------- */

QString name (int x, int y, int z)
{
    return QString::number (z) + ", " + QString::number (y)+ "," + QString::number (x);
}

void GameBoard::addPawn (int x, int y, int z, QColor c)
{
    QPolygonF p = board [z][y][x]->polygon ();

    QPointF sum (0, 0);
    int cnt = p.count();
    for (int i = 0; i < cnt; i++)
        sum += p.at(i);
    sum /= cnt;

    QGraphicsEllipseItem * e = new QGraphicsEllipseItem;
    e->setPos (sum + QPointF (20, 20)); // !?
    e->setRect (-40, -40, 40, 40);
    e->setPen (QColor ("blue"));
    e->setBrush (c);
    e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);

    QGraphicsTextItem * t = new QGraphicsTextItem;
    t->setPlainText ("\u2659");

    QFont f = t->font ();
    f.setPointSize (f.pointSize() + 16);
    t->setFont (f);
    t->setPos (-40, -50);

    t->setParentItem (e);

    scene->addItem (e);
}

void GameBoard::addPoint (QPointF p)
{
    qreal x = p.x();
    qreal y = p.y();
    QGraphicsEllipseItem * e = new QGraphicsEllipseItem;
    e->setPos (x, y);
    e->setRect (-10, -10, 10, 10);
    e->setPen (QColor ("blue"));
    e->setBrush (QColor ("orange"));
    e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
    scene->addItem (e);
}

void GameBoard::line (QPointF A, QPointF B, QPointF AB[N+1])
{
    for (int i = 0; i <= N; i++)
        AB[i] = A + (B-A) * i / N;
}

void GameBoard::drawSection (QPointF AB[N+1], QPointF CD[N+1])
{
    for (int i = 0; i <= N; i++)
        scene->addLine (AB[i].x(), AB[i].y(), CD[i].x(), CD[i].y(), QColor ("orange"));
}

QPointF intersection (QPointF A, QPointF B, QPointF C, QPointF D)
{
    // primka AB  p(t) = A+t*(B-A)
    // primka CD  q(u) = C+u*(D-C)
    QLineF AB (A, B);
    QLineF CD (C, D);
    QPointF result;
    QLineF::IntersectType answer = AB.intersects(CD, & result);
    assert (answer != QLineF::NoIntersection);
    return result;
}

/* ---------------------------------------------------------------------- */

void GameBoard::draw (QGraphicsScene * p_scene)
{
    scene = p_scene;

    // scene->setSceneRect (-320, -320, 320, 320);
    // scene->setSceneRect (0, 0, 640, 640);

    // scene->addLine (-640, 0, 640, 0, QColor ("red"));
    // scene->addLine (0, -640, 0, 640, QColor ("red"));

    QPointF Z = QPoint (0, 0);
    QPointF A = rot30 (0, a*4, 0);
    QPointF B = rot30 (0, a*4/sqrt3_2, 1);
    QPointF C = rot30 (0, a*4, 2);

    QPointF ZA [N+1]; line (Z, A, ZA);
    QPointF AB [N+1]; line (A, B, AB);
    QPointF CB [N+1]; line (C, B, CB);
    QPointF ZC [N+1]; line (Z, C, ZC);

    QPointF V[N+1][N+1];
    for (int i = 0; i <= N; i++)
        for (int k = 0; k <= N; k++)
            V[i][k] = intersection (ZA[k], CB[k], AB[i], ZC[i]);

    for (int z = 0; z < 2; z++)
        for (int y = 0; y < N; y++)
            for (int x = 0; x < 2*N; x++)
                board [z][y][x] = nullptr;

    for (int t = 0; t < 6; t++)
    {
        QPointF W[N+1][N+1];
        for (int i = 0; i <= N; i++)
            for (int k = 0; k <= N; k++)
                W[i][k] = rot30 (V[i][k], 2*t);

        for (int i = 0; i < N; i++)
        {
            for (int k = 0; k < N ; k++)
            {
                QGraphicsPolygonItem * box = new QGraphicsPolygonItem;
                QPolygonF polygon;
                polygon << W [i]  [k];
                polygon << W [i+1][k];
                polygon << W [i+1][k+1];
                polygon << W [i]  [k+1];
                box->setPolygon (polygon);

                box->setPen (QColor ("blue"));
                if ((i+k+t)%2 == 1)
                   box->setBrush (QColor ("cornflowerblue"));
                else
                   box->setBrush (QColor ("yellow"));

                box->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
                scene->addItem (box);

                int z = ((t+1)/2) % 3;
                int x;
                if (t%2 == 0)
                    x = N+i;
                else
                    x = N-1-i;
                int y = N-1-k;

                if (t%2 == 1)
                   std::swap (x, y);

                // cout << z << " " << y << " " << x << endl;
                assert (z >= 0 && z < 3);
                assert (y >= 0 && y < N);
                assert (x >= 0 && x < 2*N);
                board [z][y][x] = box;
                box->setToolTip (name (x, y, z));
            }
        }
    }

    for (int x = 0; x < 2*N; x++)
    {
        addPawn (x, 1, 0, QColor ("blue"));
        addPawn (x, 1, 1, QColor ("red"));
        addPawn (x, 1, 2, QColor ("gray"));
    }
}

/* ---------------------------------------------------------------------- */

void drawBoard (QGraphicsScene * scene)
{
    GameBoard obj;
    obj.draw (scene);
}

/* ---------------------------------------------------------------------- */

