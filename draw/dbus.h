/* dbus.h */

#ifndef DBUS_H
#define DBUS_H

#include <QObject>
#include <QTextEdit>
#include <QGraphicsScene>

/* ---------------------------------------------------------------------- */

class Receiver : public QObject
{
   Q_OBJECT
   Q_CLASSINFO ("D-Bus Interface", "org.example.ReceiverInterface")

   public:
      explicit Receiver (QWidget * parent, QTextEdit * p_info, QGraphicsScene * p_scene);
      ~ Receiver();

   public slots:
       Q_SCRIPTABLE void hello (QString s);
       Q_SCRIPTABLE void navigateToSlot (const QString & objectName, const QString & signalSignature, const QStringList & parameterNames);

   private:
      QTextEdit * info;
      QGraphicsScene * scene;
};

/* ---------------------------------------------------------------------- */

void callHello (QString msg);
void callNavigateToSlot (const QString & objectName, const QString & signalSignature, const QStringList & parameterNames);

/* ---------------------------------------------------------------------- */

#endif // DBUS_H
