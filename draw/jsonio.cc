#include "jsonio.h"
#include "io.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <QFile>
#include <QFileDialog>

#include <QApplication>
#include <QClipboard>
#include <QMimeData>
#include <QMessageBox>
#include <QString>

/* ---------------------------------------------------------------------- */

QColor getColor (QString name, QString default_name)
{
    if (QColor::isValidColor (name))
       return QColor (name);
    else
       return QColor (default_name);
}

/* ---------------------------------------------------------------------- */

void readItems (QJsonObject obj, QGraphicsScene * scene,  QGraphicsItem * target);

void readItem (QJsonObject obj, QGraphicsScene * scene,  QGraphicsItem * target)
{
    QString type = obj ["type"].toString ();
    QGraphicsItem * item = createItem (type);
    if (item != nullptr)
    {
       item->setToolTip (obj ["name"].toString ());

       int x = obj ["x"].toInt ();
       int y = obj ["y"].toInt ();
       item->setPos (x, y);

       if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
       {
          QColor c = getColor (obj ["pen"].toString (),  "red");
          QColor d = getColor (obj ["brush"].toString (), "yellow");

          shape->setPen (c);
          shape->setBrush (d);

          if (QGraphicsRectItem * r = dynamic_cast < QGraphicsRectItem * > (shape))
          {
              int w = obj ["width"].toInt (100);
              int h = obj ["height"].toInt (80);
              r->setRect (0, 0, w, h);
          }

          if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
          {
              int w = obj ["width"].toInt (100);
              int h = obj ["height"].toInt (80);
              e->setRect (0, 0, w, h);
          }
       }

       if (QGraphicsLineItem * t = dynamic_cast < QGraphicsLineItem * > (item))
       {
           int w = obj ["width"].toInt (100);
           int h = obj ["height"].toInt (80);
           t->setLine (0, 0, w, h);
       }

       readItems (obj, scene, item); // read inner items, add to this item
       setupItem (item); // set movable flag

       if (target != nullptr)
          item->setParentItem (target);
       else
          scene->addItem (item);
    }
}

void readItems (QJsonObject obj, QGraphicsScene * scene,  QGraphicsItem * target)
{
    if (obj.contains ("items") && obj ["items"].isArray())
    {
        QJsonArray list = obj ["items"].toArray ();
        for (QJsonValue item : list)
        {
            if (item.isObject ())
               readItem (item.toObject (), scene, target);
        }
    }
}

/* ---------------------------------------------------------------------- */

QJsonObject writeItem (QGraphicsItem * item)
{
    QJsonObject obj;

    obj ["type"] = itemType (item);
    obj ["name"] = item->toolTip ();

    obj ["x"] = item->x();
    obj ["y"] = item->y();

    if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
    {
        obj ["pen"] = penToString (shape->pen());
        obj ["brush"] = brushToString (shape->brush());

        if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
        {
           obj ["width"]  = e->rect().width();
           obj ["height"] = e->rect().height();
        }

        if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
        {
           obj ["width"]  = e->rect().width();
           obj ["height"] = e->rect().height();
        }

       if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (shape))
       {
            obj ["width"]  = e->line().dx();
            obj ["height"] = e->line().dy();
        }
    }

    QJsonArray list;
    for (QGraphicsItem * t : item->childItems ())
    {
        QJsonObject v = writeItem (t);
        list.append (v);
    }
    obj ["items"] = list;

    return obj;
}

/* ---------------------------------------------------------------------- */

void readJson (QByteArray code, QGraphicsScene * scene,  QGraphicsItem * target)
{
    QJsonDocument doc = QJsonDocument::fromJson (code);
    QJsonObject obj = doc.object ();
    readItems (obj, scene, target);
}

QByteArray writeJson (QGraphicsScene * scene)
{
    QJsonObject obj;
    QJsonArray list;
    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
    {
       if (item->parentItem() == nullptr)
          list.append (writeItem (item));
    }
    obj ["items"] = list;

    QJsonDocument doc (obj);
    QByteArray code = doc.toJson ();
    return code;
}

/* ---------------------------------------------------------------------- */

