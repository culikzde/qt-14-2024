
/* python-engine.h */

#ifndef PYTHON_ENGINE_H
#define PYTHON_ENGINE_H

#include <string>
using std::string;

void add_sample_module ();
void test_sample_module ();

void run_python_string (string cmd);
void run_python_file (string fileName);

void init_python_module ();
void finish_python_module ();

#endif // PYTHON_ENGINE_H
