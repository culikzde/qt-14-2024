#ifndef COLORBUTTON_H
#define COLORBUTTON_H

#include <QToolButton>

class ColorButton : public QToolButton
{
public:
    ColorButton (QString p_name);
    QString name;
    QColor color;
    QPixmap getImage ();
    void mousePressEvent (QMouseEvent *event) override;
};

#endif // COLORBUTTON_H
