
/* svg.h */

#ifndef SVG_H
#define SVG_H

// requires Qt >= 4.4
// requires QtNetwork QtSvg

#include <QSvgWidget>
#include <QNetworkReply>

class SvgView : public QSvgWidget
{
   Q_OBJECT

   public:
      SvgView (QWidget * parent = NULL);

   public slots:
      void replyFinished (QNetworkReply*);
};

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // SVG_H

