#include "tree.h"
#include "ui_tree.h"
#include <QApplication>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

#include <QDir>
#include <QDateTime>

void MainWindow::displayDir (QTreeWidgetItem * branch, QString path)
{
    QDir dir (path);
    QFileInfoList list = dir.entryInfoList (QDir::AllEntries | QDir::NoDot | QDir::NoDotDot,
                                            QDir::DirsFirst | QDir::Name);
    for (QFileInfo info : list)
    {
        QTreeWidgetItem * item = new QTreeWidgetItem;
        item->setText (0, info.fileName());
        item->setToolTip (0, info.absoluteFilePath());
        if (info.isDir())
        {
           item->setForeground (0, QColor ("red"));
           displayDir (item, info.absoluteFilePath());
        }
        else
        {
           item->setForeground (0, QColor ("blue"));
           item->setText (1, QString::number (info.size()));
           item->setText (2, info.lastModified().toString ("yyyy-MM-dd HH:mm:ss"));
        }
        branch->addChild (item);
    }
}

void MainWindow::on_actionRun_triggered()
{
    ui->tree->setHeaderLabels (QStringList () << "name" << "size" << "time");

    QDir dir0 ("..");
    QDir dir (dir0.absolutePath());

    QTreeWidgetItem * top = new QTreeWidgetItem;
    top->setText (0, dir.dirName());
    top->setToolTip (0, dir.absolutePath());
    top->setForeground (0, QColor ("red"));
    ui->tree->addTopLevelItem (top);
    displayDir (top,  dir.absolutePath());

    ui->tree->expandAll();
}

void MainWindow::on_tree_itemDoubleClicked (QTreeWidgetItem *item, int column)
{
    QString fileName = item->toolTip (0);

    QFile f (fileName);
    if (f.open (QFile::ReadOnly))
    {
       ui->textEdit->setText (f.readAll());
    }
    f.close ();
}

void MainWindow::on_actionQuit_triggered()
{
    close ();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

